@extends('layouts.install', ['no_header' => 1])
@section('title', 'Welcome - POS Installation')

@section('content')
<div class="container">
    <div class="row">
        <h1 class="page-header text-center">{{ config('app.name', 'POS') }}</h1>

        <div class="col-md-8 col-md-offset-2">
          <h3 class="text-success">Bagus! <br/>Instalasi Aplikasi Anda berhasil</h3>
          <hr>
          <p>Semua detail aplikasi disimpan di file <b>.env</b>. Anda dapat mengubahnya kapan saja di file tersebut.</p>

          <p>Mulailah dengan mendaftarkan bisnis Anda <a href="{{route('business.getRegister')}}">disini</a>.</p>
        </div>
    </div>
</div>
@endsection
